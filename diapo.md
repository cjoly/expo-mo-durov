% Vkontakte
% L. Fenouillet & C. Joly
% 2017

<!-- TODO Aspects partie prennantes externes politique russe dans partie Durov -->

# Présentation de VKontakte (VK)

## Qu’est-ce

 + Facebook russe

<a title="By Christallkeks Pixelsnader [CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3AMost_popular_social_networking_sites_by_country.svg"><img width="512" alt="Most popular social networking sites by country" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Most_popular_social_networking_sites_by_country.svg/512px-Most_popular_social_networking_sites_by_country.svg.png"/></a>

<small>By Christallkeks Pixelsnader [CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0)], via Wikimedia Commons</small>

---------------

## Essort

![](https://cjoly.gitlab.io/expo-mo-durov/vk_user.png)
<small>Based on Okras (Own work) [CC BY-SA 3.0 (https://creativecommons.org/licenses/by-sa/3.0)], via Wikimedia Commons</small>

---------------

## Historique

 + Créé en 2006 par Pavel Durov
 + Mail.ru possède 100% des parts aujourd'hui
 + Problème de copyright: Gala Records en 2012 et blockage de VK en Italie en novembre 2013
 + Démarche qualité : accord avec Universal Music

# Caractérisation de Vkontakte

## Type d'organisation

 + Privée à but lucratif

---------------

## Finalités

 + Économique
 + Sociale
 + Assurer sa pérennité

---------------

## Nature de l'activité et but

 + Secteur économique tertiaire
 + Site web de réseautage social Russe

--------------

### Services proposés

 + Envoi et réception de messages privés
 + Diffusions d'actualités personalisées
 + Gestion de communautés
 + Partage d'opinion et système de like
 + Vie privée
 + Synchronisation avec d'autres services de communication
 + Traduction de la plate-forme en 83 langues

## Status juridique

 + Démarrée comme société à responsabilité limité (SARL) jusqu’en 2013. Depuis filiale de Mail.ru.

--------------

## Ressources

 + $133 million de revenu en 2016 (d'après Mail.ru Group Financial Statements 2016)
 + Plateforme internet importante
 + Bureaux à Moscou et St Petersbourg.

---------------

## Champs d'action géographique


<a title="By Christallkeks Pixelsnader [CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3AMost_popular_social_networking_sites_by_country.svg"><img width="512" alt="Most popular social networking sites by country" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Most_popular_social_networking_sites_by_country.svg/512px-Most_popular_social_networking_sites_by_country.svg.png"/></a>

<small>By Christallkeks Pixelsnader [CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0)], via Wikimedia Commons</small>

---------------

## Taille

 + PME : environ 100 salariés selon https://vk.com/about

---------------

## Nationalité

 + Russe

---------------

## RSE

 + Salle cinema et salle de sport dans les bureaux à Moscou
 + Investissement dans l'éducation (Universités) par Mail.ru
 + Contribution à des organisations caritatives à travers Dobro Mail.ru

# Fondateur & équipe de direction actuelle

-------------

## Pavel Durov

<a title="By NickLubushko [CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3APavel_Durov_sitting_portrait.jpg"><img width="256" alt="Pavel Durov sitting portrait" src="https://upload.wikimedia.org/wikipedia/commons/9/99/Pavel_Durov_sitting_portrait.jpg"/></a>

<small>By NickLubushko [CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0)], via Wikimedia Commons</small>

-----------

### Biographie

-----------

 + Naissance : 10 octobre 1984, Saint-Petersbourg
 + Père : professeur de Philologie
 + Enfance : Turin, Italie
 + Frère : Nikolai (programmation)
 + Diplôme de Philologie en 2006
 + 2006 : VKontakte

--------------

## Personnalité

 + Protection de la vie privée
 + Défense de la liberté d’expression
 + Sens du devoir

-------------------------

## Valeurs & contexte

 + Mandats FSB
 + Déc. 2011 : ordre de censurer la page d’Alexei Navalny
 + Mail.ru (prise de contrôle progressive)
 + Déc. 2013 : mandat service secret (opposants ukrainiens à la Russie)
 + Mars 2014 : deuxième demande du FSB

------------

 + Jan. 2014 : vente des 12 %
 + Avr. 2014 : exil

------------

### Opposants ukrainiens

![https://vk.com/wall1_45621](https://cjoly.gitlab.io/expo-mo-durov/wall1_45621.png)

-----------

December 13, 2013 FSB demanded from us to give the **personal data** of organizers of the groups Euromayday. Our response has been and remains a categorical refusal – Russian jurisdiction does not extend to Ukrainian users Vkontakte. The issuance of personal data Ukrainians Russian authorities would not only be a violation of law but also a **betrayal of all those millions of people in Ukraine** who have trusted us.

------------

In the process **I had to sacrifice a lot**. In particular, I sold my share of Vkontakte, since its presence could prevent me to make the right decisions. But I do not regret anything – protection of **personal data** of people worth it and much more. **Since December 2013 I have no property, but I have something more important – a clean conscience and ideals that I am willing to defend**.

--------

### Navalny

![https://vk.com/wall1_45623](https://cjoly.gitlab.io/expo-mo-durov/wall1_45623.png)

-----------

March 13, 2014 office of public Prosecutor has demanded from me to close the anti-corruption group **Alexei Navalny** under threat of blocking Vkontakte. But I have not closed the group in December 2011 and, of course, closed now.

----------

Over the past weeks, I was under pressure from different sides. A variety of methods, I managed to win more than a month, but now the time has come to say – neither I nor my team are going to exercise political censorship. We will not delete any of the anti-corruption community Navalny, nor hundreds of other communities, blocking them from us. The freedom to disseminate information is an inalienable right of a postindustrial society. **This right, without which the existence of Vkontakte has no meaning**.

-------------------

### Parties prenantes

 + Client : vrai préoccupation du client
 + Salariés : conditions favorables
 + Gouvernement russe

-----------------

### Exil

 + Service personnel 
 + Telegram

-------

#### Ressources humaines

<blockquote class="twitter-tweet" data-conversation="none" data-lang="en"><p lang="en" dir="ltr">During our team&#39;s 1-week visit to the US last year we had two attempts to bribe our devs by US agencies + pressure on me from the FBI.</p>&mdash; Pavel Durov (@durov) <a href="https://twitter.com/durov/status/873868773119451136?ref_src=twsrc%5Etfw">June 11, 2017</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

## Nouvelle direction

 + Boris Dobrodeev
 + Fils de Oleg Dobrodeyev, patron d’un groupe de media publique
 + Proche du pouvoir
 + Démarche de qualité : accords

# Conclusion

--------

## Qualités de manager

 + Vision stratégique
 + Loyauté des hommes

## Écueils

 + Dépassé par vision entreprenariale (VKontakte)
 + Facteurs de contingeance externes : politique russe

## Politique russe

 + Blocage en Ukraine
 + Élection présidentielles russes (mars 2018)

--------

### Références

 + https://www.bloomberg.com/profiles/people/18631989-boris-dobrodeyev
 + Mail.ru financial statement : https://corp.imgsmail.ru/media/files/mail.rugrouparfy2016.pdf
 + https://vk.com/about
 
-----------------

 + https://www.bloomberg.com/research/stocks/private/people.asp?privcapId=61468363 & https://www.bloomberg.com/profiles/companies/0184776D:RU-vkontakte-llc
 + https://www.washingtonpost.com/world/in-new-sanctions-list-ukraine-blocks-russian-social-media-sites/2017/05/16/a982ab4e-3a16-11e7-9e48-c4f199710b69_story.html?utm_term=.83d52b9c4e7c
 + https://en.wikipedia.org/wiki/VK_(social_networking)

## Questions
